# mazurov


## WORKFLOW


## Connect git repository to Kubernetes cluster and create dedicated namespace

```
helm upgrade --install kubeagent gitlab/gitlab-agent \
    --namespace fabrique-prod \
    --create-namespace \
    --set image.tag=v15.6.0 \
    --set config.token=************** \
    --set config.kasAddress=wss://kas.gitlab.com

```    
kubectl config set-context --current --namespace fabrique-prod

## Automatic SSL certificate
https://cert-manager.io/  
```
kubectl apply -f k8s/domain/cert-fabrique-tk.yaml  
kubectl get certificate  
kubectl get event  
kubectl get challenges
```

## Create DEV STAGING PROD enviroment **AUTOMATICAMENTE**
https://kustomize.io/

```
cd k8s/environments
kubectl kustomize overlays/dev > ../../manifest/prod/dev.yaml
kubectl kustomize overlays/staging > ../../manifest/prod/staging.yaml
kubectl kustomize overlays/prod > ../../manifest/prod/prod.yaml
```

## Deployment done by **GitOps** tecnology 
https://about.gitlab.com/topics/gitops/  
after any changes in manifest DIR related ENVIROMENT will be Upgraded **AUTOMATICAMENTE** 

## Persisten volume
manifest/prod/pvc.yaml  
(based on **longhorn deistributed** file system)  
https://longhorn.io/


## **Utils:** Copy static files to Persisten volume
```
kubectl apply -f utils/pod_for_migration.yaml   
kubectl exec -it ubuntu -- /bin/bash  
kubectl cp  *******/dist ubuntu:longhorndisk -c ubuntu  
kubectl delete -f utils/pod_for_migration.yaml
```

curl https://fabrique.tk